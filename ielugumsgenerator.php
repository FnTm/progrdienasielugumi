<?php
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class IelugumsGenerator extends TCPDF {
    //Page header
    public function Header() {
        $bMargin = $this->getBreakMargin();
        $auto_page_break = $this->AutoPageBreak;
        $this->SetAutoPageBreak(false, 0);
        $img_file = 'fons.jpg';
        $this->Image($img_file, 0, 0, 0, 0, '', '', '', false, 300, '', false, false, 0);
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
    }
}

function GenerateInvite($filename, $personname) {
	
	$pdf = new IelugumsGenerator("L", PDF_UNIT, "A5", true, 'UTF-8', false);
	
	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('LU DF');
	$pdf->SetTitle('Ielūgums uz programmētāju dienu');
	$pdf->SetSubject('LU programmētāju diena 2010');
	$pdf->SetKeywords('LU, DF, ielūgums');
	
	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	
	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	
	//set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(0);
	$pdf->SetFooterMargin(0);
	
	// remove default footer
	$pdf->setPrintFooter(false);
	
	//set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	//set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
	
	//set some language-dependent strings
	$pdf->setLanguageArray($l);
	
	// ---------------------------------------------------------
	
	// set font
	$pdf->SetFont('dejavusans', '', 48);
	
	// add a page
	$pdf->AddPage();
	
	// Print a text
$html = '<p stroke="0.2" fill="true" strokecolor="black" color="black" style="font-weight:bold;font-size:30pt;text-align:center;"><br />'.$personname.'</p>';
	$pdf->writeHTML($html, true, false, true, false, '');
	
	
	// ---------------------------------------------------------
	
	//Close and output PDF document
	$pdf->Output($filename, 'F');
}
//============================================================+
// END OF FILE                                                
//============================================================+